/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package binarios;


public class Suma_Binaria {
    

public  String Suma_Binaria(String binario1, String binario2) {
    if (binario1 == null || binario2== null) 
    {
        return "";
    }
    int primer_elemento =binario1.length() - 1;
    int segundo_elemento = binario2.length() - 1;
    StringBuilder sb = new StringBuilder();
    int acarreado = 0;
    while (primer_elemento >= 0 || segundo_elemento >= 0) {
        int sum = acarreado;
        if (primer_elemento >= 0) {
            sum += binario1.charAt(primer_elemento) - '0';
            primer_elemento--;
        }
        if (segundo_elemento >= 0) {
            sum +=binario2.charAt(segundo_elemento) - '0';
            segundo_elemento--;
        }
        acarreado = sum >> 1;
        sum = sum & 1;
        sb.append(sum == 0 ? '0' : '1');
    }
    if (acarreado > 0){
        
        sb.append('1');
    }

    sb.reverse();
    return String.valueOf(sb);
}

    
     public static void main(String []A)
    {
    Suma_Binaria e= new Suma_Binaria();
 System.out.println( e.Suma_Binaria("1000", "10"));
    }
    

    
}  
   