/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package binarios;

public class Binario_Decimal{

      public int conversion_decimal (char c){
         switch (c) {
           case '0'  : return 0;
           case '1'  : return 1;
           default   : return 0;
         }
     }	
	 int Convertidor (String binario ){
         int a = 0;
         int n = binario.length();
         for (int i = 0; i < n; i++) {
           char c = binario.charAt(i);
           a *= 2;
           a += conversion_decimal(c);
         }
        return a;
     }
     public static void main(String[] args) {
       
       Binario_Decimal e = new Binario_Decimal();
       System.out.println(e.Convertidor("101"));
       
     }
}